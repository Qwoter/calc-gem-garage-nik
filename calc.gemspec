# -*- encoding: utf-8 -*-
lib = File.expand_path('../lib', __FILE__)
$LOAD_PATH.unshift(lib) unless $LOAD_PATH.include?(lib)
require 'calc/version'

Gem::Specification.new do |gem|
  gem.name          = "calc-nik"
  gem.version       = Calc::VERSION
  gem.authors       = ["Nikita Kovaliov"]
  gem.email         = ["laqwoter@gmail.com"]
  gem.description   = %q{Basic calculator}
  gem.summary       = %q{Basic calculator}
  gem.homepage      = ""

  gem.extensions << "ext/calc/extconf.rb"

  gem.add_development_dependency "rspec"
  gem.add_development_dependency "simplecov"
  gem.add_development_dependency "rake"

  gem.rubyforge_project = "calc-nik"

  gem.files         = `git ls-files`.split($/)
  gem.executables   = gem.files.grep(%r{^bin/}).map{ |f| File.basename(f) }
  gem.test_files    = gem.files.grep(%r{^(test|spec|features)/})
  gem.require_paths = ["lib"]
end
