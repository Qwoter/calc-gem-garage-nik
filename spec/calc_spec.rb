#calc_spec.rb
require 'spec_helper'
require './lib/calc'

describe Calc do
  before(:each) do
    @c = Calc::Calc.new
    @c.get [1, 2], 3
    @c.get [4, [5, 6], 7]

    @c_short = Calc::Calc.new
    @c_short.get 1
  end

  it "gets parameters" do
    @c.instance_variable_get("@numbers").should eql([1, 2, 3, 4, 5, 6, 7])
    @c_short.instance_variable_get("@numbers").should eql([1])
  end

  it "sums stack parameters and wipe them" do
    @c.plus.should == 28
    @c_short.plus.should == 1
    @c.instance_variable_get("@numbers").should == []
  end

  it "subsctract sum of stack from 0" do
    @c.minus.should == -28
    @c_short.minus.should == -1
    @c.instance_variable_get("@numbers").should == []
  end
end

describe Calc_c do
  it "Checks for simple palindromes" do
    Calc_c.palindrom?("abbba").should eql(true)
    Calc_c.palindrom?("abbea").should eql(false)
  end
end
